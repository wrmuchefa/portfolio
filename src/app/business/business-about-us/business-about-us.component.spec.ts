import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessAboutUsComponent } from './business-about-us.component';

describe('BusinessAboutUsComponent', () => {
  let component: BusinessAboutUsComponent;
  let fixture: ComponentFixture<BusinessAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
