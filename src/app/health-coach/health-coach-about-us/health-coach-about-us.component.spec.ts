import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCoachAboutUsComponent } from './health-coach-about-us.component';

describe('HealthCoachAboutUsComponent', () => {
  let component: HealthCoachAboutUsComponent;
  let fixture: ComponentFixture<HealthCoachAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthCoachAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCoachAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
