import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IndexComponent } from './photographer/index/index.component';
import { AboutUsComponent } from './photographer/about-us/about-us.component';
import { ContactUsComponent } from './photographer/contact-us/contact-us.component';
import { PortfolioComponent } from './photographer/portfolio/portfolio.component';
import { ResumeComponent } from './photographer/resume/resume.component';


  import { BusinessIndexComponent } from './business/business-index/business-index.component';
  import { BusinessAboutUsComponent } from './business/business-about-us/business-about-us.component';
  import { BusinessContactUsComponent } from './business/business-contact-us/business-contact-us.component';
  import { BusinessPortfolioComponent } from './business/business-portfolio/business-portfolio.component';
  import { BusinessResumeComponent } from './business/business-resume/business-resume.component';
  import { ModalAboutUsComponent } from './modal/modal-about-us/modal-about-us.component';
  import { ModalContactUsComponent } from './modal/modal-contact-us/modal-contact-us.component';
  import { ModalIndexComponent } from './modal/modal-index/modal-index.component';
  import { ModalPortfolioComponent } from './modal/modal-portfolio/modal-portfolio.component';
  import { ModalResumeComponent } from './modal/modal-resume/modal-resume.component';
  import { DoctorAboutUsComponent } from './doctor/doctor-about-us/doctor-about-us.component';
  import { DoctorContactUsComponent } from './doctor/doctor-contact-us/doctor-contact-us.component';
  import { DoctorIndexComponent } from './doctor/doctor-index/doctor-index.component';
  import { DoctorPortfolioComponent } from './doctor/doctor-portfolio/doctor-portfolio.component';
  import { DoctorResumeComponent } from './doctor/doctor-resume/doctor-resume.component';
  import { SecurityAboutUsComponent } from './security/security-about-us/security-about-us.component';
  import { SecurityContactUsComponent } from './security/security-contact-us/security-contact-us.component';
  import { SecurityIndexComponent } from './security/security-index/security-index.component';
  import { SecurityPortfolioComponent } from './security/security-portfolio/security-portfolio.component';
  import { SecurityResumeComponent } from './security/security-resume/security-resume.component';
  import { HealthCoachAboutUsComponent } from './health-coach/health-coach-about-us/health-coach-about-us.component';
  import { HealthCoachContactUsComponent } from './health-coach/health-coach-contact-us/health-coach-contact-us.component';
  import { HealthCoachIndexComponent } from './health-coach/health-coach-index/health-coach-index.component';
  import { HealthCoachPortfolioComponent } from './health-coach/health-coach-portfolio/health-coach-portfolio.component';
  import { HealthCoachResumeComponent } from './health-coach/health-coach-resume/health-coach-resume.component';
  import { ElectricianAboutUsComponent } from './electrician/electrician-about-us/electrician-about-us.component';
  import { ElectricianContactUsComponent } from './electrician/electrician-contact-us/electrician-contact-us.component';
  import { ElectricianIndexComponent } from './electrician/electrician-index/electrician-index.component';
  import { ElectricianPortfolioComponent } from './electrician/electrician-portfolio/electrician-portfolio.component';
  import { ElectricianResumeComponent } from './electrician/electrician-resume/electrician-resume.component';
  import { BloggerAboutUsComponent } from './blogger/blogger-about-us/blogger-about-us.component';
  import { BloggerContactUsComponent } from './blogger/blogger-contact-us/blogger-contact-us.component';
  import { BloggerIndexComponent } from './blogger/blogger-index/blogger-index.component';
  import { BloggerPortfolioComponent } from './blogger/blogger-portfolio/blogger-portfolio.component';
  import { BloggerResumeComponent } from './blogger/blogger-resume/blogger-resume.component';
  import { WebDeveloperAboutUsComponent } from './web-developer/web-developer-about-us/web-developer-about-us.component';
  import { WebDeveloperContactUsComponent } from './web-developer/web-developer-contact-us/web-developer-contact-us.component';
  import { WebDeveloperIndexComponent } from './web-developer/web-developer-index/web-developer-index.component';
  import { WebDeveloperPortfolioComponent } from './web-developer/web-developer-portfolio/web-developer-portfolio.component';
  import { WebDeveloperResumeComponent } from './web-developer/web-developer-resume/web-developer-resume.component';
  import { SwimmerAboutUsComponent } from './swimmer/swimmer-about-us/swimmer-about-us.component';
  import { SwimmerContactUsComponent } from './swimmer/swimmer-contact-us/swimmer-contact-us.component';
  import { SwimmerIndexComponent } from './swimmer/swimmer-index/swimmer-index.component';
  import { SwimmerPortfolioComponent } from './swimmer/swimmer-portfolio/swimmer-portfolio.component';
  import { SwimmerResumeComponent } from './swimmer/swimmer-resume/swimmer-resume.component';
  import { DanceTrainerAboutUsComponent } from './dance-trainer/dance-trainer-about-us/dance-trainer-about-us.component';
  import { DanceTrainerContactUsComponent } from './dance-trainer/dance-trainer-contact-us/dance-trainer-contact-us.component';
  import { DanceTrainerIndexComponent } from './dance-trainer/dance-trainer-index/dance-trainer-index.component';
  import { DanceTrainerPortfolioComponent } from './dance-trainer/dance-trainer-portfolio/dance-trainer-portfolio.component';
  import { DanceTrainerResumeComponent } from './dance-trainer/dance-trainer-resume/dance-trainer-resume.component';
  import { NewsAnchorAboutUsComponent } from './news-anchor/news-anchor-about-us/news-anchor-about-us.component';
  import { NewsAnchorContactUsComponent } from './news-anchor/news-anchor-contact-us/news-anchor-contact-us.component';
  import { NewsAnchorIndexComponent } from './news-anchor/news-anchor-index/news-anchor-index.component';
  import { NewsAnchorPortfolioComponent } from './news-anchor/news-anchor-portfolio/news-anchor-portfolio.component';
  import { NewsAnchorResumeComponent } from './news-anchor/news-anchor-resume/news-anchor-resume.component';
  import { YoutubeAboutUsComponent } from './youtube/youtube-about-us/youtube-about-us.component';
  import { YoutubeContactUsComponent } from './youtube/youtube-contact-us/youtube-contact-us.component';
  import { YoutubeIndexComponent } from './youtube/youtube-index/youtube-index.component';
  import { YoutubePortfolioComponent } from './youtube/youtube-portfolio/youtube-portfolio.component';
  import { YoutubeResumeComponent } from './youtube/youtube-resume/youtube-resume.component';
  import { BlogComponent } from './pages/blog/blog.component';
  import { BlogSingleComponent } from './pages/blog-single/blog-single.component';
  import { Page404Component } from './page404/page404.component';

const routes: Routes = [
          {path: '', component: IndexComponent},
          {path: 'index', component: IndexComponent},
          {path: 'index-photographer', component: IndexComponent},
          {path: 'about-us-photographer', component: AboutUsComponent},
          {path: 'contact-us-photographer', component: ContactUsComponent},
          {path: 'portfolio-photographer', component: PortfolioComponent},
          {path: 'resume-photographer', component: ResumeComponent},
          
          
          {path: 'about-us-business-man', component: BusinessAboutUsComponent},
          {path: 'contact-us-business-man', component: BusinessContactUsComponent},
          {path: 'index-business-man', component: BusinessIndexComponent},
          {path: 'portfolio-business-man', component: BusinessPortfolioComponent},
          {path: 'resume-business-man', component: BusinessResumeComponent},
          
          
          {path: 'about-us-modal', component: ModalAboutUsComponent},
          {path: 'contact-us-modal', component: ModalContactUsComponent},
          {path: 'index-modal', component: ModalIndexComponent},
          {path: 'portfolio-modal', component: ModalPortfolioComponent},
          {path: 'resume-modal', component: ModalResumeComponent},
          
          
          {path: 'about-us-doctor', component: DoctorAboutUsComponent},
          {path: 'contact-us-doctor', component: DoctorContactUsComponent},
          {path: 'index-doctor', component: DoctorIndexComponent},
          {path: 'portfolio-doctor', component: DoctorPortfolioComponent},
          {path: 'resume-doctor', component: DoctorResumeComponent},
          
          
          {path: 'about-us-security', component: SecurityAboutUsComponent},
          {path: 'contact-us-security', component: SecurityContactUsComponent},
          {path: 'index-security', component: SecurityIndexComponent},
          {path: 'portfolio-security', component: SecurityPortfolioComponent},
          {path: 'resume-security', component: SecurityResumeComponent},
          
          
          {path: 'about-us-health-coach', component: HealthCoachAboutUsComponent},
          {path: 'contact-us-health-coach', component: HealthCoachContactUsComponent},
          {path: 'index-health-coach', component: HealthCoachIndexComponent},
          {path: 'portfolio-health-coach', component: HealthCoachPortfolioComponent},
          {path: 'resume-health-coach', component: HealthCoachResumeComponent},
          
          
          {path: 'about-us-electrician', component: ElectricianAboutUsComponent},
          {path: 'contact-us-electrician', component: ElectricianContactUsComponent},
          {path: 'index-electrician', component: ElectricianIndexComponent},
          {path: 'portfolio-electrician', component: ElectricianPortfolioComponent},
          {path: 'resume-electrician', component: ElectricianResumeComponent},
          
          
          {path: 'about-us-blogger', component: BloggerAboutUsComponent},
          {path: 'contact-us-blogger', component: BloggerContactUsComponent},
          {path: 'index-blogger', component: BloggerIndexComponent},
          {path: 'portfolio-blogger', component: BloggerPortfolioComponent},
          {path: 'resume-blogger', component: BloggerResumeComponent},
          
          
          {path: 'about-us-web-developer', component: WebDeveloperAboutUsComponent},
          {path: 'contact-us-web-developer', component: WebDeveloperContactUsComponent},
          {path: 'index-web-developer', component: WebDeveloperIndexComponent},
          {path: 'portfolio-web-developer', component: WebDeveloperPortfolioComponent},
          {path: 'resume-web-developer', component: WebDeveloperResumeComponent},
          
          
          {path: 'about-us-web-developer', component: WebDeveloperAboutUsComponent},
          {path: 'contact-us-web-developer', component: WebDeveloperContactUsComponent},
          {path: 'index-web-developer', component: WebDeveloperIndexComponent},
          {path: 'portfolio-web-developer', component: WebDeveloperPortfolioComponent},
          {path: 'resume-web-developer', component: WebDeveloperResumeComponent},
          
          
          {path: 'about-us-swimmer', component: SwimmerAboutUsComponent},
          {path: 'contact-us-swimmer', component: SwimmerContactUsComponent},
          {path: 'index-swimmer', component: SwimmerIndexComponent},
          {path: 'portfolio-swimmer', component: SwimmerPortfolioComponent},
          {path: 'resume-swimmer', component: SwimmerResumeComponent},
          
          
          {path: 'about-us-dance-trainer', component: DanceTrainerAboutUsComponent},
          {path: 'contact-us-dance-trainer', component: DanceTrainerContactUsComponent},
          {path: 'index-dance-trainer', component: DanceTrainerIndexComponent},
          {path: 'portfolio-dance-trainer', component: DanceTrainerPortfolioComponent},
          {path: 'resume-dance-trainer', component: DanceTrainerResumeComponent},
          
          
          {path: 'about-us-news-anchor', component: NewsAnchorAboutUsComponent},
          {path: 'contact-us-news-anchor', component: NewsAnchorContactUsComponent},
          {path: 'index-news-anchor', component: NewsAnchorIndexComponent},
          {path: 'portfolio-news-anchor', component: NewsAnchorPortfolioComponent},
          {path: 'resume-news-anchor', component: NewsAnchorResumeComponent},
          
          
          {path: 'about-us-youtube', component: YoutubeAboutUsComponent},
          {path: 'contact-us-youtube', component: YoutubeContactUsComponent},
          {path: 'index-youtube', component: YoutubeIndexComponent},
          {path: 'portfolio-youtube', component: YoutubePortfolioComponent},
          {path: 'resume-youtube', component: YoutubeResumeComponent},
          
          
          {path: 'blog', component: BlogComponent},
          {path: 'blog-single', component: BlogSingleComponent},
		  
          {path: '**', component: Page404Component},
          
      ];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
