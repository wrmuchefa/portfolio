import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorContactUsComponent } from './doctor-contact-us.component';

describe('DoctorContactUsComponent', () => {
  let component: DoctorContactUsComponent;
  let fixture: ComponentFixture<DoctorContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
