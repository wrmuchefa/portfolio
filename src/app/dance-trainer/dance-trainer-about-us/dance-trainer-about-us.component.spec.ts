import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanceTrainerAboutUsComponent } from './dance-trainer-about-us.component';

describe('DanceTrainerAboutUsComponent', () => {
  let component: DanceTrainerAboutUsComponent;
  let fixture: ComponentFixture<DanceTrainerAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanceTrainerAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanceTrainerAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
