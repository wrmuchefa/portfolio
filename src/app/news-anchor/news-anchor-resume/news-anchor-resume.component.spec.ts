import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnchorResumeComponent } from './news-anchor-resume.component';

describe('NewsAnchorResumeComponent', () => {
  let component: NewsAnchorResumeComponent;
  let fixture: ComponentFixture<NewsAnchorResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsAnchorResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnchorResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
