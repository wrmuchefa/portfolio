import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmerAboutUsComponent } from './swimmer-about-us.component';

describe('SwimmerAboutUsComponent', () => {
  let component: SwimmerAboutUsComponent;
  let fixture: ComponentFixture<SwimmerAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwimmerAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmerAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
