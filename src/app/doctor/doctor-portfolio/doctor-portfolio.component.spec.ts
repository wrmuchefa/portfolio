import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorPortfolioComponent } from './doctor-portfolio.component';

describe('DoctorPortfolioComponent', () => {
  let component: DoctorPortfolioComponent;
  let fixture: ComponentFixture<DoctorPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
