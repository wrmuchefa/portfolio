import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-electrician-index',
  templateUrl: './electrician-index.component.html',
  styleUrls: ['./electrician-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ElectricianIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-5.png",
                  profile:"electrician"
            };
            
   member : any = {
        image:"",
        logo:"pic7.jpg",
        designation:"electrician",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"Thomas Grady",
        study:"",
        highes_degree:"",
        profile:"electrician"
  };


  constructor() { }

  ngOnInit(): void {
	  (function ($) {
      /* setTimeout(function(){ */
        jQuery("body").addClass("electrician-bnr");
      /* }, 500);   */
      
  })(jQuery);
  
  }

}
