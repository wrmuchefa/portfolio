import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-swimmer-index',
  templateUrl: './swimmer-index.component.html',
  styleUrls: ['./swimmer-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SwimmerIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-4.png",
                  profile:"swimmer"
            };
            
   member : any = {
        image:"",
        logo:"pic10.jpg",
        designation:"swimmer",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"Ella Grady",
        study:"",
        highes_degree:"",
        profile:"swimmer"
  };


  constructor() { }

  ngOnInit(): void {
	  (function ($) {
      /* setTimeout(function(){ */
        
        

           try {
		jQuery('.ripples-water').ripples({
			resolution: 512,
			dropRadius: 20, //px
			perturbance: 0.04,
		});
		jQuery('.main').ripples({
			resolution: 512,
			dropRadius: 10, //px
			perturbance: 0.01,
			interactive: false
		});
	}
	catch (e) {
		jQuery('.error').show().text(e);
	}

	jQuery('.js-ripples-disable').on('click', function() {
		jQuery('body, .main').ripples('destroy');
		jQuery(this).hide();
	});

	jQuery('.js-ripples-play').on('click', function() {
		jQuery('.ripples-water, .main').ripples('play');
	});

	jQuery('.js-ripples-pause').on('click', function() {
		jQuery('.ripples-water, .main').ripples('pause');
	});

	// Automatic drops
	setInterval(function() {
		var $el = jQuery('.main');
		var x = Math.random() * $el.outerWidth();
		var y = Math.random() * $el.outerHeight();
		var dropRadius = 20;
		var strength = 0.04 + Math.random() * 0.04;

		$el.ripples('drop', x, y, dropRadius, strength);
	}, 400);
      
      
        
        
        
        
        
      /* }, 500);   */
      
  })(jQuery);
  
  }

}
