import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricianAboutUsComponent } from './electrician-about-us.component';

describe('ElectricianAboutUsComponent', () => {
  let component: ElectricianAboutUsComponent;
  let fixture: ComponentFixture<ElectricianAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricianAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricianAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
