import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnchorContactUsComponent } from './news-anchor-contact-us.component';

describe('NewsAnchorContactUsComponent', () => {
  let component: NewsAnchorContactUsComponent;
  let fixture: ComponentFixture<NewsAnchorContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsAnchorContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnchorContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
