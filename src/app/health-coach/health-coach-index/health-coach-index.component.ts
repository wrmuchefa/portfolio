import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-health-coach-index',
  templateUrl: './health-coach-index.component.html',
  styleUrls: ['./health-coach-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HealthCoachIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-3.png",
                  profile:"health-coach"
            };
            
   member : any = {
        image:"",
        logo:"pic6.jpg",
        designation:"Health coach",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"Thomas Grady",
        study:"",
        highes_degree:"",
        profile:"health-coach"
  };


  constructor() { }

  ngOnInit(): void {
	  (function ($) {
      
      /* setTimeout(function(){ */
        jQuery("#kenburn").slippry({
           transition: 'kenburns',
           useCSS: false,
           speed: 3000,
           pause: 3000,
           auto: true,
           kenZoom: 105,
           preload: 'visible',
           autoHover: false
        });
      /* }, 500);   */
        
        
    })(jQuery);
  
  }

}
