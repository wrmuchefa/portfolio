import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorResumeComponent } from './doctor-resume.component';

describe('DoctorResumeComponent', () => {
  let component: DoctorResumeComponent;
  let fixture: ComponentFixture<DoctorResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
