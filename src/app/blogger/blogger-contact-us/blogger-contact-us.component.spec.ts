import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerContactUsComponent } from './blogger-contact-us.component';

describe('BloggerContactUsComponent', () => {
  let component: BloggerContactUsComponent;
  let fixture: ComponentFixture<BloggerContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BloggerContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BloggerContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
