import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-news-anchor-index',
  templateUrl: './news-anchor-index.component.html',
  styleUrls: ['./news-anchor-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewsAnchorIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-2.png",
                  profile:"news-anchor"
            };
            
   member : any = {
        image:"",
        logo:"pic12.jpg",
        designation:"News Anchor",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"Thomas Grady",
        study:"",
        highes_degree:"",
        profile:"news-anchor"
  };


  constructor() { }

  ngOnInit(): void {
  }

}
