import { Component, OnInit } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-dance-trainer-contact-us',
  templateUrl: './dance-trainer-contact-us.component.html',
  styleUrls: ['./dance-trainer-contact-us.component.css']
})
export class DanceTrainerContactUsComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-black-2.png",
                  profile:"dance-trainer"
            };

  page_banner : any = {
        title:"Contact Us",
        profile:"dance-trainer",
  };

  constructor() { }

    ngOnInit(): void {
      (function ($) {
        setTimeout(function(){
            if(jQuery('.dezPlaceAni').length) {
            
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('focus',function(){
                $(this).parents('.form-group, .news-box').addClass('focused');
              });
              
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('blur',function(){
                var inputValue = $(this).val();
                if ( inputValue == "" ) {
                $(this).removeClass('filled');
                $(this).parents('.form-group, .news-box').removeClass('focused');  
                } else {
                $(this).addClass('filled');
                }
              })
            }
        }, 500);
      })(jQuery);
    
    }

}
