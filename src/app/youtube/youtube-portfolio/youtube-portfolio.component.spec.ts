import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubePortfolioComponent } from './youtube-portfolio.component';

describe('YoutubePortfolioComponent', () => {
  let component: YoutubePortfolioComponent;
  let fixture: ComponentFixture<YoutubePortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YoutubePortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubePortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
