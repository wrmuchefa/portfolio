import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerAboutUsComponent } from './blogger-about-us.component';

describe('BloggerAboutUsComponent', () => {
  let component: BloggerAboutUsComponent;
  let fixture: ComponentFixture<BloggerAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BloggerAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BloggerAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
