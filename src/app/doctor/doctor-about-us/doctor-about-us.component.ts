import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-about-us',
  templateUrl: './doctor-about-us.component.html',
  styleUrls: ['./doctor-about-us.component.css']
})
export class DoctorAboutUsComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-black-2.png",
                  profile:"doctor"
            };

  page_banner : any = {
        title:"About us",
        profile:"doctor",
  };
            
  page_info : any = {
                title:"What I Do",
                image:"pic4.jpg",
                description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam, pellentesque enim ac, faucibus tortor. Nulla odio nibh, cursus sit amet urna id, dignissim euismod augue."
          };

  member : any = {
        image:"",
        logo:"",
        designation:"",
        age:"29",
        dob:"",
        residence:"USA",
        address:"88 SOME STREET, SOME TOW",
        email:"EMAIL@EXAMPLE.COM",
        phone:"+0123 123 456 789",
        skype:"ALEX.SMITH",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"",
        study:"",
        highes_degree:"",
        profile:"doctor"
  };
  
          
  services : any = [
                  {
                    title:"Tooth Extraction",
                    description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
                  },
                  {
                    title:"Women Health",
                    description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
                  },
                  {
                    title:"Orthodontics",
                    description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
                  },
                  {
                    title:"Dental Fillings",
                    description:"Praesent ut tortor consectetur, semper sapien non, lacinia augue. Aenean arcu libero, facilisis et nisi non, tempus faucibus tortor. Mauris vel nulla aliquam.",
                  },
              ];
  fun_facts : any = [
                  {
                    number:"2325",
                    title:"HAPPY CLIENTS",
                  },
                  {
                    number:"7582",
                    title:"WORKING HOURS",
                  },
                  {
                    number:"12",
                    title:"AWARDS WON",
                  },
                  {
                    number:"2000",
                    title:"PROJECTS COMPLETED",
                  },
              ];
  testimonials : any = [
                  {
                    client_name:"jonathan smith",
                    image:"pic1.jpg",
                    designation:"Dentist",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
                  {
                    client_name:"fahima zerin",
                    image:"pic3.jpg",
                    designation:"Cardiologiest",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
                  {
                    client_name:"alex morfiee",
                    image:"pic2.jpg",
                    designation:"Nephrology",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
                  {
                    client_name:"rebecca martin",
                    image:"pic1.jpg",
                    designation:"Orthopaedics",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
                  {
                    client_name:"jonathan smith",
                    image:"pic3.jpg",
                    designation:"Dentist",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
                  {
                    client_name:"rebecca martin",
                    image:"pic2.jpg",
                    designation:"Cardiologiest",
                    description:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look.",
                  },
              ];
  our_clients : any = [
                  {
                    client_title:"",
                    logo:"logo5.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo3.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                   client_title:"",
                    logo:"logo1.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo2.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo6.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo4.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo5.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo6.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo1.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo2.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo6.jpg",
                    link:"javascript:void(0);",
                  },
                  {
                    client_title:"",
                    logo:"logo4.jpg",
                    link:"javascript:void(0);",
                  },
              ];
              
  constructor() { }

  ngOnInit(): void {
  }

}
