import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanceTrainerResumeComponent } from './dance-trainer-resume.component';

describe('DanceTrainerResumeComponent', () => {
  let component: DanceTrainerResumeComponent;
  let fixture: ComponentFixture<DanceTrainerResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanceTrainerResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanceTrainerResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
