import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanceTrainerPortfolioComponent } from './dance-trainer-portfolio.component';

describe('DanceTrainerPortfolioComponent', () => {
  let component: DanceTrainerPortfolioComponent;
  let fixture: ComponentFixture<DanceTrainerPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanceTrainerPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanceTrainerPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
