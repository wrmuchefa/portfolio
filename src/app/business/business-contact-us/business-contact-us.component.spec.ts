import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessContactUsComponent } from './business-contact-us.component';

describe('BusinessContactUsComponent', () => {
  let component: BusinessContactUsComponent;
  let fixture: ComponentFixture<BusinessContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
