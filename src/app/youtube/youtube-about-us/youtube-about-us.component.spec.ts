import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeAboutUsComponent } from './youtube-about-us.component';

describe('YoutubeAboutUsComponent', () => {
  let component: YoutubeAboutUsComponent;
  let fixture: ComponentFixture<YoutubeAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YoutubeAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
