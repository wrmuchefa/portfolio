import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Header1Component } from './elements/header/header1/header1.component';
import { PageBannerComponent } from './elements/banner/page-banner/page-banner.component';
import { SocialBarComponent } from './elements/social-bar/social-bar.component';
import { MyServicesComponent } from './elements/my-services/my-services.component';
import { FunFactCounterComponent } from './elements/fun-fact-counter/fun-fact-counter.component';
import { TestimonialComponent } from './elements/testimonial/testimonial.component';
import { OurClientComponent } from './elements/our-client/our-client.component';
import { TimelineComponent } from './elements/timeline/timeline.component';
import { CircleComponent } from './elements/progress-bar/circle/circle.component';
import { LineComponent } from './elements/progress-bar/line/line.component';
import { TagsComponent } from './elements/tags/tags.component';
import { IconBoxComponent } from './elements/icon-box/icon-box.component';
import { LoadingComponent } from './elements/loading/loading.component';
import { IndexComponent } from './photographer/index/index.component';
import { AboutUsComponent } from './photographer/about-us/about-us.component';
import { ContactUsComponent } from './photographer/contact-us/contact-us.component';
import { PortfolioComponent } from './photographer/portfolio/portfolio.component';
import { ResumeComponent } from './photographer/resume/resume.component';
import { BusinessIndexComponent } from './business/business-index/business-index.component';
import { BusinessAboutUsComponent } from './business/business-about-us/business-about-us.component';
import { BusinessContactUsComponent } from './business/business-contact-us/business-contact-us.component';
import { BusinessPortfolioComponent } from './business/business-portfolio/business-portfolio.component';
import { BusinessResumeComponent } from './business/business-resume/business-resume.component';
import { ModalAboutUsComponent } from './modal/modal-about-us/modal-about-us.component';
import { ModalContactUsComponent } from './modal/modal-contact-us/modal-contact-us.component';
import { ModalIndexComponent } from './modal/modal-index/modal-index.component';
import { ModalPortfolioComponent } from './modal/modal-portfolio/modal-portfolio.component';
import { ModalResumeComponent } from './modal/modal-resume/modal-resume.component';
import { DoctorAboutUsComponent } from './doctor/doctor-about-us/doctor-about-us.component';
import { DoctorContactUsComponent } from './doctor/doctor-contact-us/doctor-contact-us.component';
import { DoctorIndexComponent } from './doctor/doctor-index/doctor-index.component';
import { DoctorPortfolioComponent } from './doctor/doctor-portfolio/doctor-portfolio.component';
import { DoctorResumeComponent } from './doctor/doctor-resume/doctor-resume.component';
import { SecurityAboutUsComponent } from './security/security-about-us/security-about-us.component';
import { SecurityContactUsComponent } from './security/security-contact-us/security-contact-us.component';
import { SecurityIndexComponent } from './security/security-index/security-index.component';
import { SecurityPortfolioComponent } from './security/security-portfolio/security-portfolio.component';
import { SecurityResumeComponent } from './security/security-resume/security-resume.component';
import { HealthCoachAboutUsComponent } from './health-coach/health-coach-about-us/health-coach-about-us.component';
import { HealthCoachContactUsComponent } from './health-coach/health-coach-contact-us/health-coach-contact-us.component';
import { HealthCoachIndexComponent } from './health-coach/health-coach-index/health-coach-index.component';
import { HealthCoachPortfolioComponent } from './health-coach/health-coach-portfolio/health-coach-portfolio.component';
import { HealthCoachResumeComponent } from './health-coach/health-coach-resume/health-coach-resume.component';
import { ElectricianAboutUsComponent } from './electrician/electrician-about-us/electrician-about-us.component';
import { ElectricianContactUsComponent } from './electrician/electrician-contact-us/electrician-contact-us.component';
import { ElectricianIndexComponent } from './electrician/electrician-index/electrician-index.component';
import { ElectricianPortfolioComponent } from './electrician/electrician-portfolio/electrician-portfolio.component';
import { ElectricianResumeComponent } from './electrician/electrician-resume/electrician-resume.component';
import { BloggerAboutUsComponent } from './blogger/blogger-about-us/blogger-about-us.component';
import { BloggerContactUsComponent } from './blogger/blogger-contact-us/blogger-contact-us.component';
import { BloggerIndexComponent } from './blogger/blogger-index/blogger-index.component';
import { BloggerPortfolioComponent } from './blogger/blogger-portfolio/blogger-portfolio.component';
import { BloggerResumeComponent } from './blogger/blogger-resume/blogger-resume.component';
import { WebDeveloperAboutUsComponent } from './web-developer/web-developer-about-us/web-developer-about-us.component';
import { WebDeveloperContactUsComponent } from './web-developer/web-developer-contact-us/web-developer-contact-us.component';
import { WebDeveloperIndexComponent } from './web-developer/web-developer-index/web-developer-index.component';
import { WebDeveloperPortfolioComponent } from './web-developer/web-developer-portfolio/web-developer-portfolio.component';
import { WebDeveloperResumeComponent } from './web-developer/web-developer-resume/web-developer-resume.component';
import { SwimmerAboutUsComponent } from './swimmer/swimmer-about-us/swimmer-about-us.component';
import { SwimmerContactUsComponent } from './swimmer/swimmer-contact-us/swimmer-contact-us.component';
import { SwimmerIndexComponent } from './swimmer/swimmer-index/swimmer-index.component';
import { SwimmerPortfolioComponent } from './swimmer/swimmer-portfolio/swimmer-portfolio.component';
import { SwimmerResumeComponent } from './swimmer/swimmer-resume/swimmer-resume.component';
import { DanceTrainerAboutUsComponent } from './dance-trainer/dance-trainer-about-us/dance-trainer-about-us.component';
import { DanceTrainerContactUsComponent } from './dance-trainer/dance-trainer-contact-us/dance-trainer-contact-us.component';
import { DanceTrainerIndexComponent } from './dance-trainer/dance-trainer-index/dance-trainer-index.component';
import { DanceTrainerPortfolioComponent } from './dance-trainer/dance-trainer-portfolio/dance-trainer-portfolio.component';
import { DanceTrainerResumeComponent } from './dance-trainer/dance-trainer-resume/dance-trainer-resume.component';
import { NewsAnchorAboutUsComponent } from './news-anchor/news-anchor-about-us/news-anchor-about-us.component';
import { NewsAnchorContactUsComponent } from './news-anchor/news-anchor-contact-us/news-anchor-contact-us.component';
import { NewsAnchorIndexComponent } from './news-anchor/news-anchor-index/news-anchor-index.component';
import { NewsAnchorPortfolioComponent } from './news-anchor/news-anchor-portfolio/news-anchor-portfolio.component';
import { NewsAnchorResumeComponent } from './news-anchor/news-anchor-resume/news-anchor-resume.component';
import { YoutubeAboutUsComponent } from './youtube/youtube-about-us/youtube-about-us.component';
import { YoutubeContactUsComponent } from './youtube/youtube-contact-us/youtube-contact-us.component';
import { YoutubeIndexComponent } from './youtube/youtube-index/youtube-index.component';
import { YoutubePortfolioComponent } from './youtube/youtube-portfolio/youtube-portfolio.component';
import { YoutubeResumeComponent } from './youtube/youtube-resume/youtube-resume.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BlogSingleComponent } from './pages/blog-single/blog-single.component';
import { UserCardComponent } from './elements/user-card/user-card.component';
import { CopywriteComponent } from './elements/copywrite/copywrite.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { Page404Component } from './page404/page404.component';

@NgModule({
  declarations: [
    AppComponent,
    Header1Component,
    PageBannerComponent,
    SocialBarComponent,
    MyServicesComponent,
    FunFactCounterComponent,
    TestimonialComponent,
    OurClientComponent,
    TimelineComponent,
    CircleComponent,
    LineComponent,
    TagsComponent,
    IconBoxComponent,
    IndexComponent,
    AboutUsComponent,
    ContactUsComponent,
    PortfolioComponent,
    ResumeComponent,
    LoadingComponent,
    BusinessIndexComponent,
    BusinessAboutUsComponent,
    BusinessContactUsComponent,
    BusinessPortfolioComponent,
    BusinessResumeComponent,
    ModalAboutUsComponent,
    ModalContactUsComponent,
    ModalIndexComponent,
    ModalPortfolioComponent,
    ModalResumeComponent,
    DoctorAboutUsComponent,
    DoctorContactUsComponent,
    DoctorIndexComponent,
    DoctorPortfolioComponent,
    DoctorResumeComponent,
    SecurityAboutUsComponent,
    SecurityContactUsComponent,
    SecurityIndexComponent,
    SecurityPortfolioComponent,
    SecurityResumeComponent,
    HealthCoachAboutUsComponent,
    HealthCoachContactUsComponent,
    HealthCoachIndexComponent,
    HealthCoachPortfolioComponent,
    HealthCoachResumeComponent,
    ElectricianAboutUsComponent,
    ElectricianContactUsComponent,
    ElectricianIndexComponent,
    ElectricianPortfolioComponent,
    ElectricianResumeComponent,
    BloggerAboutUsComponent,
    BloggerContactUsComponent,
    BloggerIndexComponent,
    BloggerPortfolioComponent,
    BloggerResumeComponent,
    WebDeveloperAboutUsComponent,
    WebDeveloperContactUsComponent,
    WebDeveloperIndexComponent,
    WebDeveloperPortfolioComponent,
    WebDeveloperResumeComponent,
    SwimmerAboutUsComponent,
    SwimmerContactUsComponent,
    SwimmerIndexComponent,
    SwimmerPortfolioComponent,
    SwimmerResumeComponent,
    DanceTrainerAboutUsComponent,
    DanceTrainerContactUsComponent,
    DanceTrainerIndexComponent,
    DanceTrainerPortfolioComponent,
    DanceTrainerResumeComponent,
    NewsAnchorAboutUsComponent,
    NewsAnchorContactUsComponent,
    NewsAnchorIndexComponent,
    NewsAnchorPortfolioComponent,
    NewsAnchorResumeComponent,
    YoutubeAboutUsComponent,
    YoutubeContactUsComponent,
    YoutubeIndexComponent,
    YoutubePortfolioComponent,
    YoutubeResumeComponent,
    BlogComponent,
    BlogSingleComponent,
    UserCardComponent,
    CopywriteComponent,
    Page404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CarouselModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
