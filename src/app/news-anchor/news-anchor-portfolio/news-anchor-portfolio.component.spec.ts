import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnchorPortfolioComponent } from './news-anchor-portfolio.component';

describe('NewsAnchorPortfolioComponent', () => {
  let component: NewsAnchorPortfolioComponent;
  let fixture: ComponentFixture<NewsAnchorPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsAnchorPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnchorPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
