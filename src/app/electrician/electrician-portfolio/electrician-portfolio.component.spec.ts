import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricianPortfolioComponent } from './electrician-portfolio.component';

describe('ElectricianPortfolioComponent', () => {
  let component: ElectricianPortfolioComponent;
  let fixture: ComponentFixture<ElectricianPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricianPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricianPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
