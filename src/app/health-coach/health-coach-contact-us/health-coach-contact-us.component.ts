import { Component, OnInit } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-health-coach-contact-us',
  templateUrl: './health-coach-contact-us.component.html',
  styleUrls: ['./health-coach-contact-us.component.css']
})
export class HealthCoachContactUsComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-black-5.png",
                  profile:"health-coach"
            };

  page_banner : any = {
        title:"Contact Us",
        profile:"health-coach",
  };

  constructor() { }

    ngOnInit(): void {
      (function ($) {
        setTimeout(function(){
            if(jQuery('.dezPlaceAni').length) {
            
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('focus',function(){
                $(this).parents('.form-group, .news-box').addClass('focused');
              });
              
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('blur',function(){
                var inputValue = $(this).val();
                if ( inputValue == "" ) {
                $(this).removeClass('filled');
                $(this).parents('.form-group, .news-box').removeClass('focused');  
                } else {
                $(this).addClass('filled');
                }
              })
            }
        }, 500);
      })(jQuery);
    
    }

}
