import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCoachPortfolioComponent } from './health-coach-portfolio.component';

describe('HealthCoachPortfolioComponent', () => {
  let component: HealthCoachPortfolioComponent;
  let fixture: ComponentFixture<HealthCoachPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthCoachPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCoachPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
