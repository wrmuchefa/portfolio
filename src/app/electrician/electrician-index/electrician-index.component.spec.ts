import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricianIndexComponent } from './electrician-index.component';

describe('ElectricianIndexComponent', () => {
  let component: ElectricianIndexComponent;
  let fixture: ComponentFixture<ElectricianIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricianIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricianIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
