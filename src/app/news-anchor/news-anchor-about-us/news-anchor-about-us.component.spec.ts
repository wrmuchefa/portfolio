import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnchorAboutUsComponent } from './news-anchor-about-us.component';

describe('NewsAnchorAboutUsComponent', () => {
  let component: NewsAnchorAboutUsComponent;
  let fixture: ComponentFixture<NewsAnchorAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsAnchorAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnchorAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
