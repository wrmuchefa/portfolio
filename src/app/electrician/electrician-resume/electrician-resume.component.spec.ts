import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricianResumeComponent } from './electrician-resume.component';

describe('ElectricianResumeComponent', () => {
  let component: ElectricianResumeComponent;
  let fixture: ComponentFixture<ElectricianResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricianResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricianResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
