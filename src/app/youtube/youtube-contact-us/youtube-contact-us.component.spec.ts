import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeContactUsComponent } from './youtube-contact-us.component';

describe('YoutubeContactUsComponent', () => {
  let component: YoutubeContactUsComponent;
  let fixture: ComponentFixture<YoutubeContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YoutubeContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
