import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsAnchorIndexComponent } from './news-anchor-index.component';

describe('NewsAnchorIndexComponent', () => {
  let component: NewsAnchorIndexComponent;
  let fixture: ComponentFixture<NewsAnchorIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsAnchorIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsAnchorIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
