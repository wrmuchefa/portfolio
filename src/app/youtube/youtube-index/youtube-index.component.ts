import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-youtube-index',
  templateUrl: './youtube-index.component.html',
  styleUrls: ['./youtube-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class YoutubeIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo.png",
                  profile:"youtube"
            };
            
   member : any = {
        image:"",
        logo:"pic13.jpg",
        designation:"YouTuber",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"Thomas Grady",
        study:"",
        highes_degree:"",
        profile:"youtube"
  };


  constructor() { }

  ngOnInit(): void {
  }

}
