import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectricianContactUsComponent } from './electrician-contact-us.component';

describe('ElectricianContactUsComponent', () => {
  let component: ElectricianContactUsComponent;
  let fixture: ComponentFixture<ElectricianContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElectricianContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectricianContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
