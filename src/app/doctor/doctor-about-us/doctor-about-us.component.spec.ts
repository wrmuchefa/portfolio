import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorAboutUsComponent } from './doctor-about-us.component';

describe('DoctorAboutUsComponent', () => {
  let component: DoctorAboutUsComponent;
  let fixture: ComponentFixture<DoctorAboutUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorAboutUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorAboutUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
