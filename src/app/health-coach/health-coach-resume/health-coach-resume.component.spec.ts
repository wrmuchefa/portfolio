import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCoachResumeComponent } from './health-coach-resume.component';

describe('HealthCoachResumeComponent', () => {
  let component: HealthCoachResumeComponent;
  let fixture: ComponentFixture<HealthCoachResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthCoachResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCoachResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
