import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessResumeComponent } from './business-resume.component';

describe('BusinessResumeComponent', () => {
  let component: BusinessResumeComponent;
  let fixture: ComponentFixture<BusinessResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
