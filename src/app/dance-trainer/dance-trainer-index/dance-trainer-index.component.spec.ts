import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanceTrainerIndexComponent } from './dance-trainer-index.component';

describe('DanceTrainerIndexComponent', () => {
  let component: DanceTrainerIndexComponent;
  let fixture: ComponentFixture<DanceTrainerIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanceTrainerIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanceTrainerIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
