import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerPortfolioComponent } from './blogger-portfolio.component';

describe('BloggerPortfolioComponent', () => {
  let component: BloggerPortfolioComponent;
  let fixture: ComponentFixture<BloggerPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BloggerPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BloggerPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
