import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmerPortfolioComponent } from './swimmer-portfolio.component';

describe('SwimmerPortfolioComponent', () => {
  let component: SwimmerPortfolioComponent;
  let fixture: ComponentFixture<SwimmerPortfolioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwimmerPortfolioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmerPortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
