import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeIndexComponent } from './youtube-index.component';

describe('YoutubeIndexComponent', () => {
  let component: YoutubeIndexComponent;
  let fixture: ComponentFixture<YoutubeIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YoutubeIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
