import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BloggerResumeComponent } from './blogger-resume.component';

describe('BloggerResumeComponent', () => {
  let component: BloggerResumeComponent;
  let fixture: ComponentFixture<BloggerResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BloggerResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BloggerResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
