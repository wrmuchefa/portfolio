import { Component, OnInit } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-swimmer-contact-us',
  templateUrl: './swimmer-contact-us.component.html',
  styleUrls: ['./swimmer-contact-us.component.css']
})
export class SwimmerContactUsComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-black-4.png",
                  profile:"swimmer"
            };

  page_banner : any = {
        title:"Contact Us",
        profile:"swimmer",
  };

  constructor() { }

    ngOnInit(): void {
      (function ($) {
        setTimeout(function(){
            if(jQuery('.dezPlaceAni').length) {
            
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('focus',function(){
                $(this).parents('.form-group, .news-box').addClass('focused');
              });
              
              $('.dezPlaceAni input, .dezPlaceAni textarea').on('blur',function(){
                var inputValue = $(this).val();
                if ( inputValue == "" ) {
                $(this).removeClass('filled');
                $(this).parents('.form-group, .news-box').removeClass('focused');  
                } else {
                $(this).addClass('filled');
                }
              })
            }
        }, 500);
      })(jQuery);
    
    }

}
