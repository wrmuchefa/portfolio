import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmerResumeComponent } from './swimmer-resume.component';

describe('SwimmerResumeComponent', () => {
  let component: SwimmerResumeComponent;
  let fixture: ComponentFixture<SwimmerResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwimmerResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmerResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
