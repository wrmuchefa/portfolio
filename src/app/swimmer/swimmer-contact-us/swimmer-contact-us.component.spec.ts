import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmerContactUsComponent } from './swimmer-contact-us.component';

describe('SwimmerContactUsComponent', () => {
  let component: SwimmerContactUsComponent;
  let fixture: ComponentFixture<SwimmerContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwimmerContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmerContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
