import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCoachContactUsComponent } from './health-coach-contact-us.component';

describe('HealthCoachContactUsComponent', () => {
  let component: HealthCoachContactUsComponent;
  let fixture: ComponentFixture<HealthCoachContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthCoachContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCoachContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
