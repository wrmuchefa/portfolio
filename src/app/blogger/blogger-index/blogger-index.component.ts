import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
declare  var jQuery:  any;
declare  var particlesJS:  any;

@Component({
  selector: 'app-blogger-index',
  templateUrl: './blogger-index.component.html',
  styleUrls: ['./blogger-index.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BloggerIndexComponent implements OnInit {
  
  profile : any = {
                  logo:"logo-black-2.png",
                  profile:"blogger"
            };
            
   member : any = {
        image:"",
        logo:"pic8.jpg",
        designation:"blogger",
        age:"29",
        dob:"",
        residence:"",
        address:"",
        email:"",
        phone:"",
        skype:"",
        whatsapp:"",
        about_me:"",
        intrests:"",
        name:"ella grady",
        study:"",
        highes_degree:"",
        profile:"blogger"
  };



  constructor() { }

  ngOnInit(): void {
	  (function ($) {
      setTimeout(function(){
        
        
    if($('#particles-snow').length > 0 )
	{
      particlesJS("particles-snow", {
        "particles": {
          "number": {
            "value": 400,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": "#ffffff"
          },
          "shape": {
            "type": "image",
            "stroke": {
              "width": 8,
              "color": "#fff"
            },
            "polygon": {
              "nb_sides": 10
            },
            "image": {
              "src": "assets/images/snow.png",
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.7,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 2,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 10,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 10,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": false,
            "distance": 50,
            "color": "#ffffff",
            "opacity": 0.6,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 5,
            "direction": "bottom",
            "random": true,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": true,
              "rotateX": 300,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": true,
              "mode":  "bubble"
            },
            "onclick": {
              "enable": true,
              "mode": "repulse"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 150,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 200,
              "size": 10,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200,
              "duration": 0.2
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      });
      
        
        
  }      
        
        
      }, 500);  
      
  })(jQuery);
  
  }

}
