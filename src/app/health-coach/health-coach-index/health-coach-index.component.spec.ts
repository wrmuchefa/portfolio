import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthCoachIndexComponent } from './health-coach-index.component';

describe('HealthCoachIndexComponent', () => {
  let component: HealthCoachIndexComponent;
  let fixture: ComponentFixture<HealthCoachIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HealthCoachIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthCoachIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
