import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanceTrainerContactUsComponent } from './dance-trainer-contact-us.component';

describe('DanceTrainerContactUsComponent', () => {
  let component: DanceTrainerContactUsComponent;
  let fixture: ComponentFixture<DanceTrainerContactUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanceTrainerContactUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanceTrainerContactUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
