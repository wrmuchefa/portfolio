import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeResumeComponent } from './youtube-resume.component';

describe('YoutubeResumeComponent', () => {
  let component: YoutubeResumeComponent;
  let fixture: ComponentFixture<YoutubeResumeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YoutubeResumeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubeResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
