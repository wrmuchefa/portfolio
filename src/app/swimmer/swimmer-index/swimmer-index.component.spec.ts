import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimmerIndexComponent } from './swimmer-index.component';

describe('SwimmerIndexComponent', () => {
  let component: SwimmerIndexComponent;
  let fixture: ComponentFixture<SwimmerIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwimmerIndexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimmerIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
